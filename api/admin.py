from django.contrib import admin

# Register your models here.
from .models import Post, userProfile

admin.site.register(Post)
admin.site.register(userProfile)